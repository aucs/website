all: clone build
.PHONY: clone

TEMPLATES := $(shell find . -type f -name "*.m4")

core_path := aucs

build:
	# Build files from m4 templates.
	@for file in $(TEMPLATES); do \
					echo "Building template $$file..."; \
					m4 -I "clone/$(core_path)/text" "$$file" > "$${file%.m4}"; \
	done

clone:
	# Clone the AUCS core repository for data.
	mkdir -p clone
	cd clone && [ -d "$(core_path)" ] || git clone "ssh://git@gitlab.com/aucs/$(core_path)"
